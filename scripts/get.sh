#!/bin/bash

# Wait for nearest full second
waitfor

# Log date
timestamp=`date -u +%FT%T`
echo $timestamp >>log.txt

# Capture
gphoto2 --capture-image-and-download --filename $timestamp.jpg >>log.txt
echo "Capture done"

# Create fits
echo "Creating fits file"
jpg2fits -i $timestamp.jpg -o $timestamp.fits -t $timestamp -d 0.264 -c 4171 -T 10.06 &

# Sleep
sleep 1
