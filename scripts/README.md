# Scripts
Scripts from `cbassa` in #satnogs-optical matrix.


# `get.sh`
DSLR capture based on sattools: https://gist.github.com/cbassa/c5a7c2cb1545c45659f69fc6ad047e40
https://gist.githubusercontent.com/cbassa/c5a7c2cb1545c45659f69fc6ad047e40/raw/6c5acdf5f0ada3d2ad042a809b2e1c33bb43976a/get.sh


# `find.sh`
Star finding: https://gist.github.com/cbassa/cdf9b3cd84284343fb73d798d5aefe0a
https://gist.githubusercontent.com/cbassa/cdf9b3cd84284343fb73d798d5aefe0a/raw/f6325cf929d7146b449c16b2d902b7d9b1f2ea2f/find.sh


# `id.sh`
Astrometric calibration and prediction: https://gist.github.com/cbassa/16a8cbc20b87eac32ca1b44e76a505d3
https://gist.githubusercontent.com/cbassa/16a8cbc20b87eac32ca1b44e76a505d3/raw/c338d72eb6f48735b0b5b8a85a96707652c3b9da/id.sh

The DSLR capture assumes you've set the camera exposure time to 10 seconds.


# Notes
The call to `jpg2fits -d 0.264 -T 10.06` has arguments with a delay between the gphoto2 call and the shutter actually opening, as well as the actual exposure time.
You can measure this by recording audio of the shutter opening and closing, super posed on computer generated beeps at 1 second boundaries

