wcsfit
for file in 2*.fits; do 
    if [ ! -e $file.cal ]; then
	addwcs -f $file -r test.fits -m 9 -R 20
	satid $file $file.png/png 2>/dev/null
    fi
done
